/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

let activeEnv = process.env.ACTIVE_ENV || process.env.NODE_ENV || "development"
require("dotenv").config({
  path: `.env.${activeEnv}`
})

module.exports = {
  /* Your site config here */
  siteMetadata: {
    title: 'Hello, Gatsby'
  },
  plugins: [
    `gatsby-plugin-typescript`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: "gatsby-source-wordpress", //cf:https://www.gatsbyjs.org/packages/gatsby-source-wordpress/
      options: {
        baseUrl: process.env.WP_BASE_URL,
        protocol: "https",
        restApiRoutePrefix: "wp-json",
        useACF: false,
        perPage: 5,
        concurrentRequests: 10,
        includedRoutes: [
          "**/categories",
          "**/posts",
          "**/pages",
          "**/media",
          "**/tags",
          "**/taxonomies",
          "**/users",
        ],
      },
    }
  ]
}
