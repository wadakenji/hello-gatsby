const path = require('path')
const common = require('./src/config/common.ts')

exports.createPages = async function({ actions, graphql }) {
  const {createPage} = actions

  // /posts/:pageNumberのルーティング
  const {data: pageData} = await graphql(`
    query{
      allWordpressPost(limit: 10){
        pageInfo{
          currentPage
          hasNextPage
          pageCount
          perPage
          itemCount
          hasPreviousPage
        }
      }
    }
  `)
  const pageCount = pageData.allWordpressPost.pageInfo.pageCount
  for (let i = 1; i <= pageCount; i++){
    createPage({
      path: 'posts/' + i,
      component: path.resolve(`./src/templates/posts.tsx`),
      context: {
        skip: common.POSTS_NUMBER_PER_PAGE * (i - 1),
        nextPage: {
          exists: i !== pageCount,
          path: '/posts/' + (i + 1)
        },
        currentPage: i,
        previousPage: {
          exists: i !== 1,
          path: '/posts/' + (i - 1)
        }
      }
    })
  }

  // /post/:postId のルーティング
  const {data: postData} = await graphql(`
    query{
      allWordpressPost{
        edges{
          node{
            wordpress_id
          }
        }
      }
    }
  `)
  postData.allWordpressPost.edges.forEach(edge => {
    createPage({
      path: 'post/' + edge.node.wordpress_id,
      component: path.resolve(`./src/templates/post.tsx`),
      context: {
        postId: edge.node.wordpress_id
      }
    })
  })

  // /author/:authorId/:pageNumber のルーティング
  const {data: authorData} = await graphql(`
    query{
      allWordpressWpUsers {
        edges {
          node {
            name
            wordpress_id
            authored_wordpress__POST {
              wordpress_id
            }
          }
        }
      }
    }
  `)
  authorData.allWordpressWpUsers.edges.forEach(edge => {
    const maxNumPages = Math.floor(edge.node.authored_wordpress__POST.length / common.POSTS_NUMBER_PER_PAGE) + 1
    for (let i = 1; i <= maxNumPages; i++) {
      createPage({
        path: 'author/' + edge.node.wordpress_id + '/' + i,
        component: path.resolve(`./src/templates/author.tsx`),
        context: {
          authorId: edge.node.wordpress_id,
          authorName: edge.node.name,
          skip: common.POSTS_NUMBER_PER_PAGE * (i - 1),
          nextPage: {
            exists: i !== maxNumPages,
            path: '/author/' + (i + 1)
          },
          currentPage: i,
          previousPage: {
            exists: i !== 1,
            path: '/author/' + (i - 1)
          }
        }
      })
    }
  })

  // /category/:categoryId/:pageNumber のルーティング
  const {data: categoryData} = await graphql(`
    query{
      allWordpressCategory {
        edges {
          node {
            name
            wordpress_id
            count
          }
        }
      }
    }
  `)
  categoryData.allWordpressCategory.edges.forEach(edge => {
    const maxNumPages = Math.floor(edge.node.count / common.POSTS_NUMBER_PER_PAGE) + 1
    for (let i = 1; i <= maxNumPages; i++) {
      createPage({
        path: 'category/' + edge.node.wordpress_id + '/' + i,
        component: path.resolve(`./src/templates/category.tsx`),
        context: {
          categoryId: edge.node.wordpress_id,
          categoryName: edge.node.name,
          skip: common.POSTS_NUMBER_PER_PAGE * (i - 1),
          nextPage: {
            exists: i !== maxNumPages,
            path: '/category/' + (i + 1)
          },
          currentPage: i,
          previousPage: {
            exists: i !== 1,
            path: '/category/' + (i - 1)
          }
        }
      })
    }
  })
}