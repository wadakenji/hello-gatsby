import * as React from 'react'
import {Link, graphql} from 'gatsby'

export default (prop: PaginationProp) => {
  return (
    <div className="pagination">
      <div className="prev">{prop.previousPage.exists && <Link to={prop.previousPage.path}>{'prev'}</Link>}</div>
      <div><span className="currentPage">{prop.currentPage}</span></div>
      <div className="next">{prop.nextPage.exists && <Link to={prop.nextPage.path}>{'next'}</Link>}</div>
    </div>
  )
}

interface PaginationProp {
  nextPage: {
    exists: boolean,
    path: string
  },
  currentPage: number,
  previousPage: {
    exists: boolean,
    path: string
  }
}