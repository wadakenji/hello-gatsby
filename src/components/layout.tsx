import * as React from "react"

import Header from './header'
import Footer from './footer'
import Container from './container'
import SEO from './SEO'

import "../styles/global.css"

export default ({children}: { children: any }) => {
  return (
    <React.Fragment>
      <SEO/>
      <Header></Header>
      <Container>
        {children}
      </Container>
      <Footer></Footer>
    </React.Fragment>
  )}