import * as React from 'react'
import {Link} from 'gatsby'

const common = require('../config/common')

export default (prop: Prop) => {
  return (
    <div className="post-in-list">
      <Link to={prop.link} className="post-in-list-eye-catch">
        <img src={prop.eyeCatch || common.NO_IMAGE_URL} alt="Eye Catch"/>
      </Link>
      <div className="post-in-list-right">
        <Link to={prop.link}>
          <h4 className="post-in-list-title">{prop.title}</h4>
        </Link>
        <div className="post-in-list-date-author">
          <p>Date {prop.date}</p>
          <p>by
            <Link to={prop.author.link}> {prop.author.name}</Link>
          </p>
        </div>
        <div className="post-in-list-excerpt" dangerouslySetInnerHTML={{__html: prop.excerpt}}></div>
      </div>
    </div>
  )
}

interface Prop {
  title: string,
  eyeCatch?: string,
  date: string,
  author: {
    name: string,
    link: string
  },
  excerpt: string,
  link: string
}