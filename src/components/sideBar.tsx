import * as React from 'react'
import {Link, graphql, useStaticQuery} from 'gatsby'

export default () => {
  const data = useStaticQuery(
    graphql`
      query SideBarData {
        allWordpressWpUsers {
          edges {
            node {
              name
              wordpress_id
            }
          }
        }
        allWordpressCategory {
          edges {
            node {
              name
              wordpress_id
            }
          }
        }
      }
    `
  )

  return (
    <div className="side-bar">
      <div className="side-bar-author">
        <h4>author</h4>
        {data.allWordpressWpUsers.edges.map((e: Edge, index: number) =>
          <Link to={'/author/' + e.node.wordpress_id + '/1'} key={index}>
            {e.node.name}
          </Link>
        )}
        <Link to={'/author'} style={{textAlign:"center"}}> >> all authors </Link>
      </div>
      <div className="side-bar-category">
        <h4>category</h4>
        {data.allWordpressCategory.edges.map((e: Edge, index: number) =>
          <Link to={'/category/' + e.node.wordpress_id + '/1'} key={index}>
            {e.node.name}
          </Link>
        )}
      </div>
    </div>
  )
}

interface Edge {
  node: {
    name:string,
    wordpress_id: number
  }
}

interface Data {
  allWordpressWpUsers: {
    edges: [
      {
        node: {
          name: string,
          wordpress_id: number
        }
      }
    ]
  },
  allWordpressCategory: {
    edges: [
      {
        node: {
          name: string,
          wordpress_id: number
        }
      }
    ]
  }
}
