import * as React from "react"

export default ({children}: { children: any }) => {
  return (
  <div className="container">{children}</div>
)}