import * as React from 'react'
import {Helmet} from 'react-helmet'
import {graphql, useStaticQuery} from "gatsby";

const common = require('../config/common')

export default (prop: Prop) => {
  const {
    allWordpressSiteMetadata: {
      edges: [
        {
          node: {
            name: wordpressTitle,
            description: wordpressDescription
          }
        }
      ]
    }
  } = useStaticQuery(
    graphql`
      query {
          allWordpressSiteMetadata {
              edges {
                  node {
                      name
                      description
                  }
              }
          }
      }
    `
  )


  const meta = {
    title: prop.title ? wordpressTitle + ' - ' + prop.title : wordpressTitle,
    description: prop.description ? prop.description.replace(/<("[^"]*"|'[^']*'|[^'">])*>/g,'') : wordpressDescription,
    image: prop.image ? prop.image : common.NO_IMAGE_URL,
    url: common.BASE_URL + prop.url,
    type: prop.url === '/' ? 'website' : 'article'
  }

  return (
    <Helmet title={meta.title}>
      <meta name="description" content={meta.description}/>
      <meta name="image" content={meta.image}/>
      <meta property="og:url" content={meta.url}/>
      <meta property="og:type" content={meta.type}/>
      <meta property="og:title" content={meta.title}/>
      <meta property="og:description" content={meta.description}/>
      <meta property="og:image" content={meta.image}/>
      <meta name="twitter:card" content={'summary_large_image'}/>
      <meta name="twitter:title" content={meta.title}/>
      <meta name="twitter:description" content={meta.description}/>
      <meta name="twitter:image" content={meta.image}/>
    </Helmet>
  )
}

interface Prop {
  url: string,
  title?: string,
  description?: string,
  image?: string,
  type?: string
}