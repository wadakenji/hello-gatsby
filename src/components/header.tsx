import * as React from "react"
import {useStaticQuery, graphql, Link} from "gatsby";

export default () => {
  const data = useStaticQuery(
    graphql`
      query{
        allWordpressSiteMetadata{
          edges{
            node{
              name
              description
            }
          }
        }
      }
    `
  )

  const siteTitle = data.allWordpressSiteMetadata.edges[0].node.name
  return (
    <header>
      <div className="header-title"><Link to={'/'} style={{color: "#d7c3c7"}}>{siteTitle}</Link></div>
      <div className="header-menu">
        <Link to={'/posts/1'} className="header-link">blog</Link>
      </div>
    </header>
  )
}

interface Data {
  allWordpressSiteMetadata: {
    edges: [
      {
        node:{
          name: string,
          description: string
        }
      }
    ]
  }
}
