import * as React from 'react'
import {Link, graphql} from 'gatsby'

import Layout from '../components/layout'
import SideBar from '../components/sideBar'
import SEO from '../components/SEO'

const common = require('../config/common')

export default ({data, pageContext}: { data: Data, pageContext: PageContext }) => {
  const post = data.allWordpressPost.edges[0].node

  return (
    <Layout>
      <SEO url={'/post/' + pageContext.postId}
           title={post.title}
           description={post.excerpt}
           image={post.featured_media && post.featured_media.source_url}/>
      <div className="main-content">
        <div className="post">
          <h1 className="post-title">{post.title}</h1>
          <div className="post-date-author">
            <p>Date {post.date}</p>
            <p>by
              <Link to={'/author/' + post.author.wordpress_id + '/1'}> {post.author.name}</Link>
            </p>
          </div>
          {post.featured_media && (<div className="post-eye-catch"><img src={post.featured_media.source_url}/></div>)}
          <div className="post-content" dangerouslySetInnerHTML={{__html: post.content}}>
          </div>
          <div className="post-categories">
            カテゴリー:
            {post.categories.map((c, index) => (
              <Link to={'/category/' + c.wordpress_id + '/1'} key={index}>
                <span>{index > 1 && ','}</span>
                {c.name}
              </Link>
            ))}
          </div>
        </div>
      </div>
      <SideBar/>
    </Layout>
  )
}

interface Data {
  allWordpressPost: {
    edges: [
      {
        node: {
          title: string,
          date: string,
          featured_media: {
            source_url: string
          },
          wordpress_id: number,
          author: {
            name: string,
            wordpress_id: number
          },
          content: string,
          categories: [
            {
              name: string
              wordpress_id: number,
            }
          ]
          excerpt: string
        }
      }
    ]
  }
}

interface PageContext {
  postId: number
}

export const query = graphql`
    query post($postId: Int!) {
        allWordpressPost(filter: {wordpress_id: {eq: $postId}}) {
            edges {
                node {
                    title
                    date(formatString: "YYYY/MM/DD")
                    featured_media {
                        source_url
                    }
                    wordpress_id
                    author {
                        name
                        wordpress_id
                    }
                    content
                    categories {
                        name
                        wordpress_id
                    }
                    excerpt
                }
            }
        }
    }
`
