import * as React from 'react'
import {Link, graphql} from 'gatsby'

import Layout from '../components/layout'
import PostInList from '../components/postInList'
import SideBar from '../components/sideBar'
import Pagination from '../components/pagination'
import SEO from '../components/SEO'

export default ({data, pageContext}: { data: Data, pageContext: PageContext }) => {
  return (
    <Layout>
      <SEO url={'/posts/' + pageContext.currentPage}
           title={'posts'}/>
      <div className="main-content">
        <Pagination nextPage={pageContext.nextPage}
                    currentPage={pageContext.currentPage}
                    previousPage={pageContext.previousPage}/>
        {data.allWordpressPost.edges.map(e => (
          <PostInList title={e.node.title}
                      eyeCatch={e.node.featured_media && e.node.featured_media.source_url}
                      date={e.node.date}
                      author={{name: e.node.author.name, link: '/author/' + e.node.author.wordpress_id + '/1'}}
                      excerpt={e.node.excerpt}
                      link={'/post/' + e.node.wordpress_id}/>
        ))}
        <Pagination nextPage={pageContext.nextPage}
                    currentPage={pageContext.currentPage}
                    previousPage={pageContext.previousPage}/>
      </div>
      <SideBar/>
    </Layout>
  )
}

interface PageContext {
  skip: number,
  nextPage: {
    exists: boolean,
    path: string
  },
  currentPage: number,
  previousPage: {
    exists: boolean,
    path: string
  }
}

interface Data {
  allWordpressPost: {
    edges: [
      {
        node: {
          title: string,
          excerpt: string,
          date: string,
          featured_media: {
            source_url: string
          },
          wordpress_id: number,
          author: {
            name: string,
            wordpress_id: number
          }
        }
      }
    ]
  }
}

export const query = graphql`
  query MyQuery($skip: Int) {
    allWordpressPost(limit: 10, skip: $skip) {
      edges {
        node {
          title
          excerpt
          date(formatString: "YYYY/MM/DD")
          featured_media {
            source_url
          }
          wordpress_id
          author {
            name
            wordpress_id
          }
        }
      }
    }
  }
`
