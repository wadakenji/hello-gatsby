exports.POSTS_NUMBER_PER_PAGE = 10
exports.NO_IMAGE_URL = 'https://www.shoshinsha-design.com/wp-content/uploads/2016/10/%E3%82%B9%E3%82%AF%E3%83%AA%E3%83%BC%E3%83%B3%E3%82%B7%E3%83%A7%E3%83%83%E3%83%88-2016-10-05-0.41.12.png'
exports.BASE_URL = process.env.NODE_ENV === 'development' ? 'localhost:8000' : 'https://hello-gatsby-typescript.netlify.com'