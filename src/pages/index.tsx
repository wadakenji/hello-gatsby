import * as React from "react"
import {Link, graphql} from "gatsby"

import Layout from "../components/layout"
import SEO from '../components/SEO'

export default ({data}: { data: Data }) =>
  <Layout>
    <SEO url={'/'}/>
    <div style={{textAlign:"center", margin:"0 auto"}}>
      <h1>hello gatsby</h1>
      <div><Link to={'/about'}>about this page</Link></div>
      <div><Link to={'/posts/1'}>blog</Link></div>
    </div>
  </Layout>

interface Data {
  site: {
    siteMetadata: {
      title: string
    }
  },
  allWordpressPost: {
    edges: [
      {
        node: {
          title: string
        }
      }
    ]
  }
}


export const query = graphql`
  query{
    site{
      siteMetadata{
        title
      }
    }
    allWordpressPost{
      edges{
        node{
          title
        }
      }
    }
  }
`
