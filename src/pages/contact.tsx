import * as React from 'react'
import axios from 'axios'

import Layout from '../components/layout'
import SideBar from '../components/sideBar'
import SEO from '../components/SEO'

interface Prop {}

interface State {
  name: string,
  email: string,
  subject: string,
  message: string,
  response: string
}

interface Event {
  target: {
    value: string
  }
}

class Contact extends React.Component<Prop, State>{
  constructor (props: Prop) {
    super(props)
    this.state = {
      name: '',
      email: '',
      subject: '',
      message: '',
      response: ''
    }
  }

  handlChangeEmail = (event: Event) => {
    this.setState({email: event.target.value})
  }
  handlChangeName = (event: Event) => {
    this.setState({name: event.target.value})
  }
  handlChangeSubject = (event: Event) => {
    this.setState({subject: event.target.value})
  }
  handlChangeMessage = (event: Event) => {
    this.setState({message: event.target.value})
  }

  send = async () => {
    await axios.post('https://hello-gatsby-typescript.netlify.com/.netlify/functions/contact', {
      name: this.state.name,
      email: this.state.email,
      subject: this.state.subject,
      message: this.state.message
    })
      .then(r => {
        this.setState({response: '送信されました'})
        console.log(r)
      })
      .catch(e => {
        this.setState({response: '送信に失敗しました'})
        console.log(e)
      })
  }

  render() {
    return (
      <Layout>
        <SEO url={'/contact'}
             title={'contact'}/>
        <div className="main-content">
        <div className="contact-form">
          <h3>お問い合わせ</h3>
          <div className="item">
            <label>メールアドレス</label>
            <input type="email" onChange={this.handlChangeEmail}/>
          </div>
          <div className="item">
            <label>名前</label>
            <input type="text" onChange={this.handlChangeName}/>
          </div>
          <div className="item">
            <label>件名</label>
            <input type="text" onChange={this.handlChangeSubject}/>
          </div>
          <div className="item">
            <label>お問い合わせ内容</label>
            <textarea rows={10} onChange={this.handlChangeMessage}></textarea>
          </div>
          <div className="item">{this.state.response && this.state.response}</div>
          <button className="btn" onClick={this.send}
                  disabled={!this.state.name || !this.state.email || !this.state.subject || !this.state.message}>
            送信
          </button>
        </div>
        </div>
        <SideBar/>
      </Layout>
    )
  }

}

export default Contact
  
