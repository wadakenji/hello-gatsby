import * as React from "react"
import {Link, graphql} from "gatsby"

import Header from "../components/header"
import Layout from "../components/layout"
import SEO from '../components/SEO'

const userStyles = require('../styles/users.module.css')

interface Props {
  avatar: string,
  username: string,
  excerpt: string
}

const User = (props: Props) => (
  <div className={userStyles.user}>
    <img src={props.avatar} className={userStyles.avatar} alt="" />
    <div className={userStyles.description}>
      <h2 className={userStyles.username}>{props.username}</h2>
      <p className={userStyles.excerpt}>{props.excerpt}</p>
    </div>
  </div>
)

export default ({data}: {data: Data}) =>
    <Layout>
      <SEO title={'about'}/>
      <div style={{color: `purple`, margin:"0 auto"}}>
        <User avatar="https://images.unsplash.com/photo-1519052537078-e6302a4968d4"
              username="kenji"
              excerpt="gatsby sugoi"/>
        <User avatar="https://images.unsplash.com/photo-1513360371669-4adf3dd7dff8"
              username="wada"
              excerpt="typescript wakaran"/>
      </div>
    </Layout>

interface Data {
  site: {
    siteMetadata: {
      title: string
    }
  }
}

export const query = graphql`
  query{
    site{
      siteMetadata{
        title
      }
    }
  }
`