import * as React from "react"
import {Link, graphql} from "gatsby"

import Layout from "../components/layout"
import SideBar from "../components/sideBar"
import SEO from '../components/SEO'

export default ({data}: {data: Data}) =>
  <Layout>
    <SEO url={'/author'}
         title={'authors'}/>
    <div className="main-content">
    <div className="authors">
      <h4>Authors</h4>
      {data.allWordpressWpUsers.edges.map((e, index) => (
        <div key={index} className="author">
          <Link to={'/author/' + e.node.wordpress_id + '/1'} className="author-name">{e.node.name}</Link>
          <div className="author-description">{e.node.description}</div>
        </div>
      ))}
    </div>
    </div>
    <SideBar/>
  </Layout>

interface Data {
  allWordpressWpUsers: {
    edges: [
      {
        node: {
          name: string,
          wordpress_id: number,
          description: string
        }
      }
    ]
  }
}


export const query = graphql`
  query{
    allWordpressWpUsers {
      edges {
        node {
          name
          wordpress_id
          description
        }
      }
    }
  }
`